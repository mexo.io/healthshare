package com.uaemex.xperiafan13.healthshare.model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import java.io.Reader
import java.io.Serializable

data class BasicData(
        var sex: String = "",
        var activity: Int = 0,
        val weight: Int = 0,
        val height: Int = 0,
        val publicKey: String = ""
) : Serializable {
    class Deserializer : ResponseDeserializable<BasicData> {
        override fun deserialize(reader: Reader) = Gson().fromJson(reader, BasicData::class.java)
    }
}