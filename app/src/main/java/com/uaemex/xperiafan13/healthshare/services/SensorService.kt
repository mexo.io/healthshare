package com.uaemex.xperiafan13.healthshare.services

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import app.akexorcist.bluetotohspp.library.BluetoothSPP
import app.akexorcist.bluetotohspp.library.BluetoothState
import app.akexorcist.bluetotohspp.library.DeviceList
import com.uaemex.xperiafan13.healthshare.R

class SensorService: Activity() {
    private var bt: BluetoothSPP = BluetoothSPP(this)

    interface PulseEvent {
        fun dataReceived(string: String)
    }

    private var sensorListener: PulseEvent? = null

    fun setTheListener(listen: PulseEvent) {
        sensorListener = listen
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_devicelist)

        if (!bt.isBluetoothAvailable) {
            Toast.makeText(applicationContext, "Bluetooth is not available", Toast.LENGTH_SHORT).show()
            finish()
        }

        bt.setOnDataReceivedListener { data, message -> Log.i("DDDD", message) }

        /* bt.setBluetoothStateListener(new BluetoothSPP.BluetoothStateListener() {
               public void onServiceStateChanged(int state) {
                   if (state == BluetoothState.STATE_CONNECTED) {
                       Log.i("DDDD", "connected");
                   } else if (state == BluetoothState.STATE_CONNECTING) {
                       Log.i("DDDD", "connecting");
                   } else if (state == BluetoothState.STATE_LISTEN) {
                       Log.i("DDDD", "listen");
                   }
                   else if (state == BluetoothState.STATE_NONE) {
                       Log.i("DDDD", "none");
                   }
               }
           }); */

        val btnConnect = findViewById<Button>(R.id.btnConnect)
        btnConnect.setOnClickListener {
            if (bt.serviceState == BluetoothState.STATE_CONNECTED) {
                bt.disconnect()
            } else {
                val intent = Intent(this, DeviceList::class.java)
                intent.putExtra("bluetooth_devices", "Bluetooth devices")
                intent.putExtra("no_devices_found", "No device")
                intent.putExtra("scanning", "Scanning")
                intent.putExtra("scan_for_devices", "Search")
                intent.putExtra("select_device", "Select")
                //intent.putExtra("layout_list", R.layout.device_layout_list);
                //intent.putExtra("layout_text", R.layout.device_layout_text);
                startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE)
            }
        }
    }

    public override fun onDestroy() {
        super.onDestroy()
        //bt.stopService();
    }

    public override fun onStart() {
        super.onStart()
        if (!bt.isBluetoothEnabled) {
            bt.enable()
        } else {
            if (!bt.isServiceAvailable) {
                bt.setupService()
                bt.startService(BluetoothState.DEVICE_OTHER)
                setup()
            }
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {
            if (resultCode == Activity.RESULT_OK)
                bt.connect(data)
        } else if (requestCode == BluetoothState.REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                bt.setupService()
                Toast.makeText(applicationContext, "Bluetooth was enabled.", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(applicationContext, "Bluetooth was not enabled.", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    fun setup() {
        val btnSend = findViewById<View>(R.id.btnSend) as Button
        btnSend.setOnClickListener { bt.send("Text", true) }
    }
}