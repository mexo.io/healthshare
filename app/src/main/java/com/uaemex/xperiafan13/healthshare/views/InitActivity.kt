package com.uaemex.xperiafan13.healthshare.views

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.uaemex.xperiafan13.healthshare.R
import kotlinx.android.synthetic.main.activity_init.*
import android.content.Intent
import android.util.Log
import android.support.v4.app.ActivityOptionsCompat
import android.view.View


class InitActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_init)
        setListeners()
    }

    private fun setListeners() {
        btn_login.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, logo as View, "logo")
            startActivity(intent, options.toBundle())
        }
        btn_register.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, logo as View, "logo_register")
            startActivity(intent, options.toBundle())
        }
    }
}
