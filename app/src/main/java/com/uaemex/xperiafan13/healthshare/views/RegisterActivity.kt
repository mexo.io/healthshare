package com.uaemex.xperiafan13.healthshare.views

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.text.format.Time
import android.util.Log
import com.afollestad.materialdialogs.MaterialDialog
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.uaemex.xperiafan13.healthshare.MainActivity
import com.uaemex.xperiafan13.healthshare.R
import com.uaemex.xperiafan13.healthshare.model.Response
import com.uaemex.xperiafan13.healthshare.model.User
import kotlinx.android.synthetic.main.activity_register.*


class RegisterActivity : AppCompatActivity() {

    private var currentPassword: String = ""
    private var baseServer: String = "http://192.168.0.7:2122/user/"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        setDate()
        btn_register.setOnClickListener {
            //startActivity(Intent(this, SetUpActivity::class.java))
            checkFields()
        }
    }

    private fun checkFields() {
        val nameValue: String = name.text.toString()
        val nicknameValue: String = nickname.text.toString()
        val emailValue: String = email.text.toString()
        val passwordValue: String = password.text.toString()
        val birthdayValue: String = birthday.text.toString()
        currentPassword = passwordValue
        checkEmpty(User(
                name = nameValue.trim(),
                nickname = nicknameValue.trim(),
                email = emailValue.trim(),
                password = passwordValue.trim(),
                birthday = birthdayValue.trim()
        ))
    }

    private fun checkEmpty(data: User) {
        if (data.name != "" && data.nickname != "" && data.email != "" && data.password != "" && data.birthday != "") {
            val requestPost = "${baseServer}register".httpPost().body(Gson().toJson(data))
            requestPost.headers["Content-Type"] = "application/json"
            requestPost.responseObject(User.Deserializer()) { request, response, result ->
                if (response.statusCode == 201) {
                    update(result)
                } else {
                    MaterialDialog(this)
                            .title(text = "Error")
                            .message(text = "Un usuario con la misma información ya existe")
                            .positiveButton(text = "Aceptar")
                            .show()
                }
            }
        } else {
            MaterialDialog(this)
                    .title(text = "Error")
                    .message(text = "Porfavor llena todos los campos requeridos")
                    .positiveButton(text = "Aceptar")
                    .show()
        }
    }

    private fun update(result: Result<User, FuelError>) {
        result.fold(success = {
            login(it)
        }, failure = {
            Log.e("Error", String(it.errorData))
        })
    }

    private fun login(data: User) {
        val requestPost = "${baseServer}login".httpPost().body(Gson().toJson(User(email = data.email, password = currentPassword)))
        requestPost.headers["Content-Type"] = "application/json"
        requestPost.responseObject(Response.Deserializer()) { request, response, result ->
            result.fold(success = {
                data.token = it.token
                data.refreshToken = it.refreshToken
                saveData(data)
            }, failure = {
                Log.e("Error", String(it.errorData))
            })
        }
    }


    private fun setDate() {
        birthday.isFocusable = false
        birthday.isClickable = true
        birthday.setOnClickListener {
            val dpd = OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val s = monthOfYear + 1
                val a = dayOfMonth.toString() + "/" + s + "/" + year
                birthday.setText("" + a)
            }
            val date = Time()
            val d = DatePickerDialog(this, dpd, date.year, date.month, date.monthDay)
            d.show()
        }
    }

    private fun saveData(data: User) {
        val preferences = getSharedPreferences("data", Context.MODE_PRIVATE)
        val editor = preferences.edit()
        val json = Gson().toJson(data)
        editor.putString("user", json).apply()
        val main = Intent(this, SetUpActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra("key", data.wallet!!.publicKey)
        startActivity(main)
    }
}
